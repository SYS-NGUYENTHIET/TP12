package ex1;

import java.io.File;
import java.io.*;

public class Main {

    public static void racine() {
        File[] res = File.listRoots();
        for (File tmp : res) {
            System.out.println(tmp.toString());
        }
    }

    public static boolean test_fichier(File file) {
        boolean res;
        try {
            if (file.isDirectory()) {
                res = false;
                System.out.println(file.toString() + " est un dossier");
            } else {
                res = true;
                System.out.println(file.toString() + " est un fichier");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return res;
    }

    public static void creer_fichier(File file) {
        try {
            if (file.createNewFile()) {
                System.out.println(file.toString() + " a été créé");
            } else {
                System.out.println(file.toString() + " n'a pas été créé");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void lister(File file) {
        if (!test_fichier(file)) {
            try {
                File[] res = file.listFiles();
                for (File tmp : res) {
                    System.out.println(tmp.toString());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            System.out.println(file.toString() + " n'est pas un dossier, on ne peut pas le lister");
        }
    }

    public static String indent(int n) {
        String res = "";
        for (int i = 0; i < n; i++) {
            res += " ";
        }
        return res;
    }

    public static void lister_rec(File file) {
        lister_rec_aux(file, 0);
    }

    public static void lister_rec_aux(File file, int n) {
        System.out.println(indent(n) + file.toString());
        if (file.isDirectory()) {
            File[] res = file.listFiles();
            for (File tmp : res) {
                lister_rec_aux(tmp, n + 1);
            }
        }
    }

    // https://www.geeksforgeeks.org/different-ways-reading-text-file-java/
    public static void lire(String filename) {
        try {
            File f = new File(filename);
            if (!f.canRead()) {
                System.out.println("On ne peut pas lire le fichier " + f.toString());
            } else {
                BufferedReader br = new BufferedReader(new FileReader(f));
                String st;
                while ((st = br.readLine()) != null) {
                    System.out.println(st);
                }
                br.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        racine();
        File f = new File("./src");
        test_fichier(f);
        File g = new File("./toto");
        creer_fichier(g);
        File h = new File("../");
        lister(h);
        File i = new File("./");
        lister_rec(i);
        lire("src/ex1/Main.java");
    }
}

