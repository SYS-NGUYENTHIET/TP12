package ex2;

public class Main {
    public static void main(String[] args) {
        if (args.length != 2) {
            System.out.println("Pas le bon nombre d'arguments");
        } else {
            Copy.cp_file(args[0],args[1]);
        }
    }
}