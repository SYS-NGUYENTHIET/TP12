package ex2;

import java.io.*;
import java.nio.*;
import java.nio.file.*;
import java.util.*;
import java.lang.Byte;

public class Copy {
    public static void cp_file(String src, String dst) {
        try {
            // get the paths
            Path src_p = Paths.get(src);
            Path dst_p = Paths.get(dst);

            if (Files.isDirectory(src_p)) {
                cp_dir(src_p, dst_p);
                return;
            }

            // if dst is a directory, make the new dst
            if (Files.isDirectory(dst_p)) {
                cp_file(src, dst_p.toString() + "/" + src_p.getFileName().toString());
                return;
            }

            // if dst already exist, it is an error
            if (Files.exists(dst_p)) {
                System.out.println(dst + " existe déja");
                return;
            }

            // create the destination
            Path dst_file_p = dst_p.getFileName();

            if (!(dst_file_p.equals(dst_p) || Files.exists(dst_p.getParent()))) {
                // the destination has folders within it that doesn't exist, we create them
                createParents(dst_p.getParent());
            }
            // create the file
            dst_p = Files.createFile(dst_p);

            // create the reader from source file
            FileInputStream reader = new FileInputStream(src_p.toFile());

            // create the writer from destination file
            FileOutputStream writer = new FileOutputStream(dst_p.toFile());

            // buffer size
            int bufferSize = 4096;

            // the buffer, init with first line
            byte[] buffer = new byte[bufferSize];

            // read the first portion
            int nbRead = reader.read(buffer,0,bufferSize);

            // iterate through each portion
            while (nbRead != -1) {
                // write the portion
                writer.write(buffer,0,nbRead);

                // read the next portion
                nbRead = reader.read(buffer,0,bufferSize);
            }

            // close the reader/writer
            reader.close();
            writer.close();

            // transfer the permissions
            Files.setPosixFilePermissions(dst_p, Files.getPosixFilePermissions(src_p));

        } catch (FileAlreadyExistsException e) {
            System.out.println(dst + " already exists");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // create the parents if they don't exist
    public static void createParents(Path p)
            throws UnsupportedOperationException, FileAlreadyExistsException, IOException, SecurityException {
        if (p != null && !Files.exists(p)) {
            createParents(p.getParent());
            Files.createDirectory(p);
        }
    }

    // copy everything in a directory src to dst
    public static void cp_dir(Path src_p, Path dst_p) throws IOException {
        if (Files.exists(dst_p)) {
            // dst_p is not a directory means error
            if (!Files.isDirectory(dst_p)) {
                System.out.println(dst_p.toString() + " n'est pas un dossier");
                return;
            }

            // dst_p doesnt exist, we create it
        } else {
            Files.createDirectories(dst_p);

            // transfer right
            Files.setPosixFilePermissions(dst_p, Files.getPosixFilePermissions(src_p));
        }

        // get the list of every file under src
        File src_f = src_p.toFile();
        File[] to_be_copied = src_f.listFiles();

        // copy each file into dst
        for (File f : to_be_copied) {
            Path f_p = f.toPath();

            // resolve the new path for dst
            Path tmp_dst = dst_p.resolve(f_p.getFileName());
            cp_file(f.toString(), tmp_dst.toString());
        }
    }
}