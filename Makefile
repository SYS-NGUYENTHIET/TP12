SRC_DIR = src
BLD_DIR = classes

.PHONY = clean

### Compilation
all: $(patsubst $(SRC_DIR)/%.java,$(BLD_DIR)/%.class,$(wildcard $(SRC_DIR)/**/*.java))

$(BLD_DIR)/%.class: $(BLD_DIR) $(SRC_DIR)/%.java
	javac -cp $(SRC_DIR) $(word 2,$^) -d $(word 1,$^)

$(BLD_DIR):
	mkdir -p $(BLD_DIR)

### Exécution ###
ex1: $(BLD_DIR)/ex1/Main.class
	java -cp $(BLD_DIR) ex1.Main 

ex2: $(BLD_DIR)/ex2/Main.class
	java -cp $(BLD_DIR) ex2.Main ../TP12/in/ out/

### Clean ###
clean:
	rm -rf $(BLD_DIR)
	rm -rf out/*